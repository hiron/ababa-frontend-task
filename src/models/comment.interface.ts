import { User } from "./user.interface";

export interface comment{
    id: number;
    message: string;
    user: User;
}