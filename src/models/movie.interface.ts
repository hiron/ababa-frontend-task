import { comment } from "./comment.interface";
import { Rate } from "./rating.interface";

export interface Movie {
  id: number;
  title: string;
  description: string;
  image: string;
  movieBanner: string;
  runningTime: string;
  release: string;
  director: string;
  producer: string;
  comments?: comment[];
  rates?: Rate[];
}
