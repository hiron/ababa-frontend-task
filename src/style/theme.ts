import { Theme } from "@emotion/react";
import { createTheme } from "@mui/material";

export const theme: Theme = createTheme({
  typography: { fontSize: 12 },
  palette: {
    background: { default: "#253240", paper: "#3b4856" },
    text: { primary: "#e9eaec", secondary: "#75808c" },
    primary: {
      main: "#1b2634",
      light: "#515863",
    },
    secondary: {
      main: "#21df8c",
      contrastText: "#131212",
      light: "#3b4856",
    },
  },
});
