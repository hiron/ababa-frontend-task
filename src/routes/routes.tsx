import { Route, Routes } from "react-router-dom";
import Home from "../app/pages/Home";
import { MovieDetail } from "../app/pages/MovieDetail";
import { NotFound } from "../app/pages/NotFound";

export default () => (<Routes>
    <Route path='/' element={<Home />} />
    <Route path='/details/:id' element={<MovieDetail />} />
    <Route path='*' element={<NotFound />} />
</Routes>)