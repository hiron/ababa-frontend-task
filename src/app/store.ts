import { configureStore } from "@reduxjs/toolkit";
import movieReducer from '../features/movies/movieSlice';
import userReducer from '../features/login/loginSlice';
import registrationReducer from '../features/registration/signupSlice';
import movieDetailSlice from '../features/details/movieDetailSlice';
import ratingReducer from '../features/rate/ratingSlice';
import commentReducer from '../features/comment/commentSlice';


export const store =  configureStore({
  reducer: {
    movieList: movieReducer,
    user: userReducer,
    registration: registrationReducer,
    movieDetail: movieDetailSlice,
    rate: ratingReducer,
    comment: commentReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;