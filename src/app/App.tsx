import { Button } from "@mui/material";
import { Provider } from "react-redux";
import AppRoutes from '../routes/routes';
import {store} from './store';

export default function App() {
    return (
        <Provider store={store} >
            <AppRoutes />
        </Provider>
    )
}