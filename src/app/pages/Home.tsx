import { Grid, Typography } from "@mui/material";
import { MovieGridView } from "../../features/movies/MovieGridView";
import { Layout } from "./Layout";

export default function Home() {
    return (
        <Layout>
            <Grid container mt={4}>
                <Grid item xs={12} pb={3}>
                    <Typography fontSize={40} fontWeight={100}>
                        Ghibli Movies
                    </Typography>
                </Grid>
                <MovieGridView/>
            </Grid>
        </Layout>);
}