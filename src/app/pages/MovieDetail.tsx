import { Box, Button, CircularProgress, Divider, Grid, Paper, Typography } from "@mui/material"
import { createImmutableStateInvariantMiddleware } from "@reduxjs/toolkit";
import { useLocation, useParams } from "react-router-dom";
import { Layout } from "./Layout"
import invariant from 'tiny-invariant';
import { useAppDispatch, useAppSelector } from "../hooks";
import { useEffect } from "react";
import { getMovieDetail } from "../../features/details/movieDetailSlice";
import { Movie } from "../../models/movie.interface";
import { CalendarMonth, AccessTime, Timeline, Star, StarBorder } from "@mui/icons-material";
import RatingView from "../../common/components/RateComponent";
import { RateMovieView } from "../../features/rate/RateMovieView";
import { CommentAddView } from "../../features/comment/CommentAddView";
import { comment } from "../../models/comment.interface";

export const MovieDetail = () => {
    let params = useParams();
    invariant(params.id, 'expected the id of the film');
    let filmId = parseInt(params.id, 10);

    const { movie, loading, error } = useAppSelector((state) => state.movieDetail);
    const dispatch = useAppDispatch();

    useEffect(() => { dispatch(getMovieDetail(filmId)) }, []);

    return <Layout searchable={false}>
        {loading == "pending" ?
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <CircularProgress color="secondary" sx={{
                    position: "fixed",
                    left: "50%",
                    top: "50%",
                    transform: "translate(-50%, -50%)",
                    zIndex: 2
                }} />
            </Box>
            :
            <div>
                <Paper sx={{
                    height: 650,
                    backgroundImage: `url(${movie?.movieBanner})`,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    position: "relative"
                }}>
                    <Grid container position="relative" sx={{ height: 650 }}>
                        <Grid item xs={12} sm={6} >

                            <Box mx={7} py={1} px={3} bgcolor="#101e2f8c" position="absolute" bottom={30} left={0}>
                                <Typography variant="h3" fontWeight="bold" >
                                    {movie?.title}
                                </Typography>
                                <Box sx={{ verticalAlign: 'middle', display: "inline-flex" }} pr={2}>
                                    <CalendarMonth />
                                    {movie?.release}
                                </Box>
                                <Box sx={{ verticalAlign: 'middle', display: "inline-flex" }}>
                                    <AccessTime /> {Math.round(+movie?.runningTime / 60) + "h " + Math.round((+movie?.runningTime % 60)) + "m"}
                                </Box>
                                <Box>Director: {movie?.director}</Box>
                            </Box>

                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Box mx={7} py={1} px={3} bgcolor="#101e2f8c" position="absolute" bottom={30} right={0}>
                                <RatingView rates={movie?.rates} />
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
                <Box mt={3}>
                    <Box display="flex" justifyContent="space-between">
                        <Typography flexGrow={1} fontSize={40} fontWeight={100}>
                            Synopsis
                        </Typography>
                        <RateMovieView filmId={filmId} />
                    </Box>

                    <Typography pb={3} fontSize={14}>
                        {movie?.description}
                    </Typography>
                    <Typography pb={1} fontSize={14}>
                        Director: {movie?.director}
                    </Typography>
                    <Typography pb={3} fontSize={14}>
                        Producer:{movie?.producer}
                    </Typography>
                    <Divider />

                    <Box pt={2}>
                        <Typography flexGrow={1} fontSize={40} fontWeight={100}>
                            Comments
                        </Typography>
                        <CommentAddView filmId={filmId} />
                    </Box>

                    <Grid container>
                        <Grid item xs={12} sm={6}>
                            {
                                movie?.comments.map((d: comment, i: number) => (<Box key={i}>
                                    <Box mt={3} sx={{fontWeight:"bold", fontSize:14}}>{d.user.name}</Box>
                                    <Box color="#8e8f93" sx={{ fontStyle: "italic", fontWeight: 300 }}>{d.user.email}</Box>
                                    <Box mb={2} mt={1}>{d.message}</Box>
                                    <Divider/>
                                </Box>))
                            }
                        </Grid>
                    </Grid>
                </Box>
            </div>
        }
    </Layout>
}


