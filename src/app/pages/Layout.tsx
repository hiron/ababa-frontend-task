import { AppBar, Container, Grid, IconButton, Toolbar, Typography, Menu, } from "@mui/material"
import { Box } from "@mui/system";
import { Search as SearchIcon } from "@mui/icons-material";
import MovieFilterIcon from '@mui/icons-material/MovieFilter';
import React from "react";
import SearchInput from "../../common/components/SearchInput";
import { useAppSelector } from "../hooks";
import { LoginComponent } from "../../common/components/LoginComponent";
import { SignupComponent } from "../../common/components/SignupComponent";
import { LogoutComponent } from "../../common/components/LogoutComponent";
import { LoginState } from "../../features/login/loginSlice";

type Props = { children: React.ReactNode; searchable?: boolean; };


export const Layout: React.FC<Props> = ({ children, searchable = true }: Props) => {

    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };


    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const user: LoginState = useAppSelector(state => state.user as LoginState);


    return (
        <Grid container spacing={4}>
            <Grid item xs={12}>
                <AppBar position="sticky">
                    <Container maxWidth="xl" >
                        <Toolbar disableGutters >
                            <MovieFilterIcon fontSize="large" sx={{ display: { xs: 'none', md: 'flex' }, mr: 1, }} />
                            <Typography
                                variant="h6"
                                noWrap
                                component="a"
                                href="/"
                                sx={{
                                    mr: 2,
                                    display: { xs: 'none', md: 'flex' },
                                    fontFamily: 'monospace',
                                    fontWeight: 700,
                                    letterSpacing: '.3rem',
                                    color: 'inherit',
                                    textDecoration: 'none',
                                    fontSize: "25px"
                                }}
                            >
                                ABABA MOV.
                            </Typography>
                            {searchable ?
                                <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                                    <IconButton
                                        size="large"
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={handleOpenNavMenu}
                                        color="secondary"
                                    >
                                        <SearchIcon />
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorElNav}
                                        anchorOrigin={{
                                            vertical: 'bottom',
                                            horizontal: 'left',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'left',
                                        }}
                                        open={Boolean(anchorElNav)}
                                        onClose={handleCloseNavMenu}
                                        sx={{
                                            display: { xs: 'block', md: 'none' },
                                        }}
                                    >
                                        {/* {pages.map((page) => (
                                        <MenuItem key={page} onClick={handleCloseNavMenu}>
                                            <Typography textAlign="center">{page}</Typography>
                                        </MenuItem>
                                    ))} */}
                                        <SearchInput />
                                    </Menu>
                                </Box> : <></>
                            }
                            <MovieFilterIcon fontSize="large" sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
                            <Typography
                                variant="h5"
                                noWrap
                                component="a"
                                href=""
                                sx={{
                                    mr: 2,
                                    display: { xs: 'flex', md: 'none' },
                                    flexGrow: 1,
                                    fontFamily: 'monospace',
                                    fontWeight: 700,
                                    letterSpacing: '.3rem',
                                    color: 'inherit',
                                    textDecoration: 'none',
                                }}
                            >
                                ABABA MOV.
                            </Typography>

                            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                                {/* {pages.map((page) => (
                                    <Button
                                        key={page}
                                        onClick={handleCloseNavMenu}
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                    >
                                        {page}
                                    </Button>
                                ))} */}
                                {searchable ?
                                    <SearchInput /> :
                                    <></>}
                            </Box>

                            {user.login ?
                                <LogoutComponent /> : <Box>
                                    <SignupComponent />
                                    <LoginComponent variant="contained" />
                                </Box>}
                        </Toolbar>
                    </Container>
                </AppBar>
                <Box mx={10}>
                    {children}
                </Box>
            </Grid>
        </Grid>
    )
}