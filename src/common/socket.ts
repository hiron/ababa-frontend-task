import axios, { AxiosResponse } from "axios";

// const URL = import.meta.env.VITE_BASE_URL + ":" + import.meta.env.VITE_PORT;
// console.log(import.meta.env.VITE_BASE_URL);
const URL = "http://127.0.0.1:8000";

function get(path: string): Promise<AxiosResponse> {
    const TOKEN = localStorage.getItem("access_token") || "";

    console.log(URL + path);
    return axios.get(URL + path, {
        headers: {
            Authorization: "Bearer " + TOKEN,
            "content-type": "application/json",
        },
    });
}

function post(path: string, body: Object | string): Promise<AxiosResponse> {
    const TOKEN = localStorage.getItem("access_token") || ""; //useSelector((state) => state.login.token);

    console.log(URL + path);
    console.log(body);
    return axios.post(URL + path, body, {
        headers: {
            Authorization: "Bearer " + TOKEN,
            "content-type": "application/json",
        },
    });
}

export default () => ({
    get,
    post,
});
