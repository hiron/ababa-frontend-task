import { Box, Button, Dialog, DialogActions, DialogTitle, Tooltip } from "@mui/material"
import { useState } from "react";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { logout } from "../../features/login/loginSlice";


export const LogoutComponent = () => {
    const dispatch = useAppDispatch();
    const [open, setOpen] = useState(false);

    return (<><Box mr={2} sx={{ display: { xs: "none", sm: "flex" } }}> Hello, {localStorage.getItem('name')}</Box>
        <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Click to Logout">
                <Button variant="outlined" color="secondary" onClick={() => { setOpen(true) }}>
                    Logout
                </Button>
            </Tooltip>
            <Dialog fullWidth
                open={open}
                onClose={() => { setOpen(false) }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Are you sure, you want to logout?"}
                </DialogTitle>
                {/* <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Let Google help apps determine location. This means sending anonymous
                        location data to Google, even when no apps are running.
                    </DialogContentText>
                </DialogContent> */}
                <DialogActions>
                    <Button color="secondary" onClick={() => { setOpen(false) }}>Close</Button>
                    <Button variant="contained" color="secondary" onClick={() => { dispatch(logout(null)) }} autoFocus>
                        Logout
                    </Button>
                </DialogActions>
            </Dialog>
        </Box></>)
}