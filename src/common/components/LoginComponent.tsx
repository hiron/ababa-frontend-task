import { Close, FactoryRounded } from "@mui/icons-material";
import { Alert, Button, CircularProgress, Collapse, Divider, Fade, IconButton, Input, Modal, TextField, Tooltip } from "@mui/material";
import { Box } from "@mui/system";
import { FormEvent, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { getLogin, LoginState } from "../../features/login/loginSlice";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: '#3b4856',
    // border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};

type props = { variant: "contained" | "text" | "outlined" | undefined }

export const LoginComponent = ({ variant = "contained" }: props) => {
    const [open, setOpen] = useState(false);
    const [validEmail, setValidEmail] = useState(true);
    const [validPass, setValidPass] = useState(true);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    let user: LoginState = useAppSelector(state => state.user);
    let dispatch = useAppDispatch();

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        var validRegexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        //let email: string = event.target[0].value;
        //let pass: string = event.target[1].value;

        if (!email.match(validRegexEmail)) {
            setValidEmail(false);
        } else {
            setValidEmail(true);
        };

        if (!password.length) {
            setValidPass(false);
        } else {
            setValidPass(true);
        }

        validPass && validEmail && dispatch(getLogin({ email, password }));
    }

    return <>
        <Tooltip title="Click to Login">
            <Button variant={variant} color="secondary" onClick={() => { setOpen(true) }}>
                Login
            </Button>
        </Tooltip>
        <Modal
            hideBackdrop
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="child-modal-title"
            aria-describedby="child-modal-description"
        >
            <Box sx={{ ...style }}>
                <Box display="flex" alignItems='center'>
                    <Box flexGrow={1}>
                        <h1 id="child-modal-title" style={{ textAlign: 'center', paddingBottom: 3, fontWeight: 300, }}>Login</h1>
                    </Box>
                    <Box>
                        <IconButton onClick={() => setOpen(false)}>
                            <Close color="secondary" />
                        </IconButton>
                    </Box>
                </Box>
                <Divider color="secondary" />
                <form onSubmit={handleSubmit}>
                    <div id="child-modal-description">
                        <TextField onChange={(e) => setEmail(e.target.value)} variant="standard" error={!validEmail} helperText={validEmail ? "" : "Please give a valid email"} sx={{ my: 3 }} fullWidth placeholder="Email" inputProps={{ 'aria-label': 'description' }} />
                        <TextField onChange={(e) => setPassword(e.target.value)} variant="standard" error={!validPass} helperText={validPass ? "" : "Please give the password"} sx={{ mb: 5 }} fullWidth type="password" placeholder="Password" inputProps={{ 'aria-label': 'description' }} />
                    </div>
                    <Button type="submit" fullWidth variant="contained" color="secondary" onClick={() => { }}>Login</Button>
                </form>

                <Box mt={4} sx={{ textAlign: "center" }}>
                    <Fade
                        in={user.loading == "pending"}
                        style={{
                            transitionDelay: user.loading == "failed" || user.loading == "succeeded" ? "80ms" : "0ms",
                        }}
                        unmountOnExit
                    >

                        <CircularProgress color="secondary" />
                    </Fade>
                </Box>

                <Box>
                    <Collapse in={user.loading == "failed"}>
                        <Alert
                            severity="error"
                            variant="filled"
                            color="error"
                        >
                            The email or the password is invalid!
                            {/* {user.error} */}
                        </Alert>
                    </Collapse>
                </Box>


            </Box>
        </Modal>
    </>
}