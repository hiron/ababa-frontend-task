import { Close } from "@mui/icons-material";
import { Alert, Button, CircularProgress, Collapse, Divider, Fade, IconButton, Input, Modal, TextField, Tooltip } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState, FormEvent } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { getLogin } from "../../features/login/loginSlice";
import { signup } from "../../features/registration/signupSlice";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: '#3b4856',
    // border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};

export const SignupComponent = () => {
    const [open, setOpen] = useState(false);
    const [validName, setValidName] = useState(true);
    const [validEmail, setValidEmail] = useState(true);
    const [validConfPass, setValidConfPass] = useState(true);
    const [validPass, setValidPass] = useState(true);

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [confPassword, setConfPass] = useState('');
    const [password, setPassword] = useState('');

    let registration = useAppSelector(state => state.registration);
    let dispatch = useAppDispatch();

    useEffect(() => {
        if (registration.loading == "succeeded") {
            setTimeout(() => { setOpen(false) }, 1500)
        }
    }, [registration])

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        var validRegexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        if (!!name) {
            setValidName(true);
        } else {
            setValidName(false);
        }

        if (confPassword === password) {
            setValidConfPass(true)
        } else {
            setValidConfPass(false)
        }

        if (!email.match(validRegexEmail)) {
            setValidEmail(false);
        } else {
            setValidEmail(true);
        };

        if (confPassword !== password && password.length <= 4) {
            setValidPass(false);
        } else {
            setValidPass(true);
        }

        validName && validEmail && validPass && validConfPass && dispatch(signup({ name, email, password }));
    }

    return <>
        <Tooltip title="Click to registration" >
            <Button sx={{ marginRight: 2 }} variant="text" color="secondary" onClick={() => { setOpen(true) }}>
                Signup
            </Button>
        </Tooltip>
        <Modal
            hideBackdrop
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="child-modal-title"
            aria-describedby="child-modal-description"
        >
            <Box sx={{ ...style }}>
                <Box display="flex" alignItems='center'>
                    <Box flexGrow={1}>
                        <h1 id="child-modal-title" style={{ textAlign: 'center', paddingBottom: 3, fontWeight: 300, }}>Registration</h1>
                    </Box>
                    <Box>
                        <IconButton onClick={() => setOpen(false)}>
                            <Close color="secondary" />
                        </IconButton>
                    </Box>
                </Box>
                <Divider color="secondary" />
                <form onSubmit={handleSubmit}>
                    <div id="child-modal-description">
                        <TextField onChange={(e) => setName(e.target.value)} variant="standard" error={!validName} helperText={validName ? "" : "Please give your full name"} sx={{ my: 3 }} fullWidth placeholder="name" inputProps={{ 'aria-label': 'description' }} />
                        <TextField onChange={(e) => setEmail(e.target.value)} variant="standard" error={!validEmail} helperText={validEmail ? "" : "Please give a valid email"} sx={{ mb: 3 }} fullWidth placeholder="Email" inputProps={{ 'aria-label': 'description' }} />
                        <TextField onChange={(e) => setPassword(e.target.value)} variant="standard" error={!validPass} helperText={validPass ? "" : "Password will be longer than 4 character"} sx={{ mb: 3 }} fullWidth type="password" placeholder="Password" inputProps={{ 'aria-label': 'description' }} />
                        <TextField onChange={(e) => setConfPass(e.target.value)} variant="standard" error={!validConfPass} helperText={validConfPass ? "" : "Two password fields did not match"} sx={{ mb: 5 }} fullWidth type="password" placeholder="Confirm Password" inputProps={{ 'aria-label': 'description' }} />
                    </div>
                    <Button type="submit" fullWidth variant="contained" color="secondary" onClick={() => { }}>Signup</Button>
                </form>

                <Box mt={4} sx={{ textAlign: "center" }}>
                    <Fade
                        in={registration.loading == "pending"}
                        style={{
                            transitionDelay: registration.loading == "failed" || registration.loading == "succeeded" ? "80ms" : "0ms",
                        }}
                        unmountOnExit
                    >

                        <CircularProgress color="secondary" />
                    </Fade>
                </Box>

                <Box>
                    <Collapse in={registration.loading == "failed"}>
                        <Alert
                            severity="error"
                            variant="filled"
                            color="error"
                        >
                            The registration email is already exist
                            {/* {registration.error} */}
                        </Alert>
                    </Collapse>
                </Box>
                <Box>
                    <Collapse in={registration.loading == "succeeded"}>
                        <Alert
                            severity="success"
                            variant="filled"
                            color="success"
                        >
                            New user is created, Please Login.
                            {/* {user.error} */}
                        </Alert>
                    </Collapse>
                </Box>
            </Box>
        </Modal>
    </>
}