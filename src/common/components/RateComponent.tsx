import { Star, StarBorder } from "@mui/icons-material";
import { Box } from "@mui/material";
import { Rate } from "../../models/rating.interface";

type Props = { rates: Rate[] };

export default function RatingView({ rates = [] }: Props) {
    console.log(rates);
    let sumRate = rates.reduce((a, b) => a + (+b?.rate), 0) || 0;
    let avgRate: number = sumRate ? +(sumRate / rates.length).toFixed(1) : 0;
    console.log(avgRate);
    return (
        <Box>
            <Box>Rated by {rates?.length} people</Box>
            <Box sx={{ verticalAlign: 'middle', display: "inline-flex" }}>{
                Array.from({ length: 5 }, (_, i) => { return i + 1 > avgRate ? <StarBorder key={i} color="secondary" /> : <Star key={i} color="secondary" />; })
            }({avgRate})</Box>

        </Box>);

}