import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import socket from "../../common/socket";
import { Rate } from "../../models/rating.interface";

interface rateState {
  rate: Rate;
}

const initialState: rateState = { rate: { id: 0, rate: 0 } };

type PostReq = { rate: number; filmId: number };

export const rate = createAsyncThunk(
  "rating/rate",
  ({ rate, filmId }: PostReq) => {
    return socket()
      .post(`/film/${filmId}/rate`, { rate })
      .then((res) => res.data as Rate);
  }
);

const RateSlice = createSlice({
  name: "rating",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(rate.fulfilled, (sate, action) => {
      sate.rate = action.payload;
    });
  },
});

export default RateSlice.reducer;
