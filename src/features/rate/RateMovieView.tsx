import { Star, StarBorder } from "@mui/icons-material";
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton } from "@mui/material";
import { MouseEvent, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { rate as rateAction } from "../../features/rate/ratingSlice";
import { getMovieDetail } from "../details/movieDetailSlice";

type props = {filmId: number}
export function RateMovieView({ filmId }: props) {
    const { user, rate } = useAppSelector(({ user, rate }) => ({ user, rate }));
    const dispatch = useAppDispatch();

    const [alertOpen, setAlertOpen] = useState(false);
    const [rateOpen, setRateOpen] = useState(false);
    const [rating, setRating] = useState(Array.from({ length: 5 }, () => false));

    const updateRating = (event: MouseEvent<HTMLButtonElement>, index: number) => {
        setRating(Array.from({ length: 5 }, (d, i) => i > index ? false : true));
    }

    const rateMovieAciton = () => {
        if (user.login) {
            setRateOpen(true)
            setRating(Array.from({ length: 5 }, () => false));
        }
        else {
            setAlertOpen(true);
        }
    };

    const setRatingAction = () => {
        dispatch(rateAction({ rate: rating.filter(d => d).length, filmId }));
    };

    useEffect(() => {
        if (rate.rate.id != 0) {
            setRateOpen(false);
            //dispatch(getMovieDetail(filmId));
        }
    }, [rate]);

    return (<Box>
        <Button variant="outlined" color="secondary" onClick={rateMovieAciton}>Rate The Movie</Button>
        <Dialog fullWidth
            open={alertOpen}
            onClose={() => { setAlertOpen(false) }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                You need to login first to rate a movie.
            </DialogTitle>
            <DialogActions>
                <Button color="secondary" onClick={() => { setAlertOpen(false) }}>OK</Button>
            </DialogActions>
        </Dialog>
        <Dialog
            open={rateOpen}
            onClose={() => { setRateOpen(false) }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent>
                {
                    rating.map((d, i) => {
                        return (

                            d ?
                                (
                                    <IconButton key={"star_" + i} onClick={(e) => updateRating(e, i)} >
                                        <Star key={i} color="secondary" />
                                    </IconButton>) :
                                (<IconButton key={"StarBorder_" + i} onClick={(e) => updateRating(e, i)}>
                                    <StarBorder key={i} />
                                </IconButton>)

                        );
                    })
                }

            </DialogContent >
            <DialogActions>
                <Button disabled={rating.filter(d => d).length ? false : true} color="secondary" onClick={setRatingAction}>OK</Button>
            </DialogActions>
        </Dialog >
    </Box >);
}