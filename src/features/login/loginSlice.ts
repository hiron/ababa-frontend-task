import socket from "../../common/socket";
import {
  createAsyncThunk,
  createSlice,
  PayloadAction,
  PayloadActionCreator,
  Slice,
} from "@reduxjs/toolkit";
import { User } from "../../models/user.interface";

export interface LoginState {
  login: boolean;
  loading: "idle" | "pending" | "succeeded" | "failed";
  error: string;
}

const initialState: LoginState = {
  login: localStorage.getItem("access_token") ? true : false,
  loading: "idle",
  error: "",
};

interface AccessToken {
  access_token: string;
  name: string;
}

interface FailMessage {
  error: {
    message: string;
  };
}

export interface LoginCredential {
  email: string;
  password: string;
}

interface SignupCredential extends LoginCredential {
  name: string;
}

export const getLogin = createAsyncThunk(
  "user/login",
  ({ email, password }: LoginCredential) => {
    return socket()
      .post("/user/login", { email, password })
      .then((res) => res.data as AccessToken);
  }
);

const loginSlice: Slice = createSlice({
  name: "user",
  initialState,
  reducers: {
    logout(state: LoginState, action: PayloadAction<void>) {
      localStorage.removeItem("access_token");
      localStorage.removeItem("name");
      state.login = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getLogin.fulfilled, (state, action) => {
      state.login = true;
      state.loading = "succeeded";
      state.error = "";
      localStorage.setItem(
        "access_token",
        action.payload.access_token as string
      );
      localStorage.setItem("name", action.payload.name as string);
    });
    builder.addCase(getLogin.pending, (state) => {
      state.loading = "pending";
      state.error = "";
    });
    builder.addCase(
      getLogin.rejected,
      (state, action: any) => {
        console.log(action);
        state.login = false;
        state.loading = "failed";
        state.error = action.error.message as string;
        localStorage.removeItem("access_token");
        localStorage.removeItem("name");
      }
    );
  },
});

export default loginSlice.reducer;
export const { logout } = loginSlice.actions;
