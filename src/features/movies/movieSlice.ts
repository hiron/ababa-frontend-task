import { SportsHockeyRounded } from "@mui/icons-material";
import {
  AsyncThunk,
  createAsyncThunk,
  createSlice,
  Slice,
} from "@reduxjs/toolkit";
import socket from "../../common/socket";
import { Movie } from "../../models/movie.interface";

export const getMovies = createAsyncThunk(
  "movie/getMovies",
  (title?: string) => {
    return socket()
      .get("/film")
      .then(
        (res) =>
          res.data.filter((d: Movie) =>
            title ? d.title.toLowerCase().includes(title.toLowerCase()) : true
          ) as Movie[]
      );
  }
);

export interface movieState {
  movies: Movie[];
  loading: "idle" | "pending" | "succeeded" | "failed";
  error: string;
}

const initialState: movieState = {
  movies: [],
  loading: "idle",
  error: "",
};

const movieSlice: Slice = createSlice({
  name: "movies",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getMovies.pending, (state) => {
      state.loading = "pending";
    });
    builder.addCase(getMovies.fulfilled, (state, action) => {
      state.movies = action.payload;
      state.loading = "succeeded";
    });
    builder.addCase(getMovies.rejected, (state, action) => {
      state.error = action.error.message as string;
      state.loading = "failed";
    });
  },
});

export default movieSlice.reducer;
