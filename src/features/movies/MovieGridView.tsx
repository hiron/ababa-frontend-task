import { Box, CircularProgress, Grid, Paper, Stack, useTheme } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { Movie } from "../../models/movie.interface";
import { getMovies } from "./movieSlice";

export const MovieGridView = () => {
    const dispatch = useAppDispatch();
    const { movies, loading, error } = useAppSelector((state) => state.movieList);

    useEffect(() => { dispatch(getMovies()) }, []);

    return (
        <>
            {loading == "pending" ?
                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                    <CircularProgress color="secondary" sx={{
                        position: "fixed",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, -50%)",
                        zIndex: 2
                    }} />
                </Box>
                :
                movies.map((d: Movie) => {
                    return (
                        <Grid key={d.id} item xl={2} md={3} sm={6} xs={12} padding={2}>
                            <Link to={"/details/" + d.id}>
                                <Paper elevation={16} sx={{
                                    height: 350, 
                                    backgroundPosition: "center", 
                                    backgroundImage: `url(${d.image})`, 
                                    backgroundSize: "cover", 
                                    backgroundRepeat: "no-repeat",
                                    '&:hover': { border: `4px solid`, borderColor: (theme) => theme.palette.secondary.main }
                                }}>
                                </Paper>
                                <Box sx={{ fontSize: 12, fontWeight: 300, marginTop: 2 }}>{d.release + " . " + d.runningTime + "min"} </Box>
                                <Box sx={{ fontSize: 16, fontWeight: 400, }}>{d.title}</Box>
                            </Link>
                        </Grid>);
                })
            }

        </>
    );
}