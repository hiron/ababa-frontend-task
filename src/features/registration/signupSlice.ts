import socket from "../../common/socket";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { User } from "../../models/user.interface";

interface SignupState {
    loading: "idle" | "pending" | "succeeded" | "failed";
    error: string;
}

const initialState: SignupState = {
    loading: "idle",
    error: "",
};


interface SignupCredential {
    email: string;
    password: string;
    name: string;
}


export const signup = createAsyncThunk(
    "registration/signup",
    ({ name, email, password }: SignupCredential) => {
        return socket()
            .post("/user/registration", { name, email, password })
            .then((res) => res.data as User);
    }
);

const RegistrationSlice = createSlice({
    name: "registration",
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder.addCase(signup.fulfilled, (state, action) => {
            state.loading = "succeeded";
            state.error = "";
        });
        builder.addCase(signup.pending, (state) => {
            state.loading = "pending";
            state.error = "";
        });
        builder.addCase(signup.rejected, (state, action: any) => {
            state.loading = "failed";
            state.error = action.error.message as string;
        });
    },
});

export default RegistrationSlice.reducer;
