import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import socket from "../../common/socket";
import { comment } from "../../models/comment.interface";

interface CommentState {
  comment: comment | undefined;
  loading: "idle" | "pending" | "succeeded" | "failed";
  error: string;
}

const initialState: CommentState = {
  comment: undefined,
  loading: "idle",
  error: "",
};

interface CommnetCredention {
  filmId: number;
  comment: { message: string };
}

export const postComment = createAsyncThunk(
  "comment/postComment",
  ({ filmId, comment }: CommnetCredention) => {
    return socket()
      .post(`/film/${filmId}/comment`, comment)
      .then((res) => res.data as comment);
  }
);

const CommentSlice = createSlice({
  name: "comment",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(postComment.fulfilled, (state, action) => {
      state.comment = action.payload;
      state.loading = "succeeded";
    });
    builder.addCase(postComment.pending, (state) => {
      state.loading = "pending";
    });
    builder.addCase(postComment.rejected, (state, action) => {
      state.error = action.error.message as string;
      state.loading = "failed";
    });
  },
});

export default CommentSlice.reducer;
