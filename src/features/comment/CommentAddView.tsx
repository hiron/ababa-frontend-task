import { Box, Button, FormControl, Grid, TextareaAutosize } from "@mui/material"
import { isPending } from "@reduxjs/toolkit";
import { FormEvent, startTransition, useEffect, useState, useTransition } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks"
import { LoginComponent } from "../../common/components/LoginComponent";
import { SignupComponent } from "../../common/components/SignupComponent";
import { postComment } from "./commentSlice";

type props = { filmId: number; }

export const CommentAddView = ({ filmId }: props) => {
    const { user, comment } = useAppSelector((state => state));
    const dispatch = useAppDispatch();
    const [isPending, startTransition] = useTransition();
    const [message, setMessage] = useState('');

    const submitForm = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        startTransition(
            () => {
                dispatch(postComment({ filmId, comment: { message } }));
            })
    }

    useEffect(() => {
        if (comment.loading == "succeeded")
            location.reload();
    }, [comment])

    return <Grid container>
        <Grid item sm={12} md={6}>
            {
                !user.login ? (<Box my={3}>You need to <LoginComponent variant="text" />Or  <SignupComponent />first, to comment</Box>) :
                    (
                        <form onSubmit={submitForm} style={{ marginBottom: 18 }}>
                            <TextareaAutosize
                                onChange={(e) => setMessage(e.target.value)}
                                disabled={isPending}
                                color="primary"
                                aria-label="minimum height"
                                minRows={6}
                                placeholder="Add your Comments here"
                                style={{ width: "100%", padding: 13, backgroundColor: "#3b4856" }}
                            />
                            <Box mt={2}>
                                <Button type="submit" disabled={isPending} variant="contained" color="secondary" fullWidth >
                                    {isPending ? "Adding..." : "Add A Comment"}

                                </Button>
                            </Box>
                        </form>)
            }
        </Grid>
    </Grid>
}