import {
    createAsyncThunk,
    createSlice,
    Slice,
} from "@reduxjs/toolkit";
import socket from "../../common/socket";
import { Movie } from "../../models/movie.interface";

export const getMovieDetail = createAsyncThunk(
    "movie/getMovieDetail",
    (id: number) => {
        return socket()
            .get("/film/" + id)
            .then((res) => res.data as Movie);
    }
);

export interface movieState {
    movie: Movie | null;
    loading: "idle" | "pending" | "succeeded" | "failed";
    error: string;
}

const initialState: movieState = {
    movie: null,
    loading: "idle",
    error: "",
};

const movieDetailSlice: Slice = createSlice({
    name: "movies",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getMovieDetail.pending, (state) => {
            state.loading = "pending";
        });
        builder.addCase(getMovieDetail.fulfilled, (state, action) => {
            state.movie = action.payload;
            state.loading = "succeeded";
        });
        builder.addCase(getMovieDetail.rejected, (state, action) => {
            state.error = action.error.message as string;
            state.loading = "failed";
        });
    },
});

export default movieDetailSlice.reducer;
