## Installation
it is a dockerize dev project. you need to clone the project first.

```shell
$ git clone https://gitlab.com/hiron/ababa-frontend-task.git
$ cd ababa-frontend-task
```

After thant just run the docker compose

```sh
$ docker-compose up --build -d
```
To work properly you need to run the both backend and frontend project in the same machine.

The project will run on http://localhost:3000

## Screen

<img src="home.png"
     alt="Home page"
     style="float: left; margin-right: 10px;" />