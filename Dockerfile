FROM node:18-alpine

RUN mkdir /ababa-front

WORKDIR /ababa-front

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY package.json /ababa-front/package.json
RUN npm install

ADD . .

ENTRYPOINT ["/entrypoint.sh"]

CMD ["npm", "run", "dev"]
